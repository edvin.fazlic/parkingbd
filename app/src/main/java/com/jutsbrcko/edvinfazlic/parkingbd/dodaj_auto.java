package com.jutsbrcko.edvinfazlic.parkingbd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class dodaj_auto extends AppCompatActivity {
    private EditText marka1,model1,reg1;
    private String markaK,modelK,reg,broj;
    private boolean uslov = true,tacno = false;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_auto);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getInstance().getReferenceFromUrl("https://parkingbd-b176e.firebaseio.com/");

        Button odustani = (Button) findViewById(R.id.odustani);
        Button unos = (Button) findViewById(R.id.potvrdiUnos);
        marka1 = (EditText) findViewById(R.id.marka1);
        model1 = (EditText) findViewById(R.id.model1);
        reg1 = (EditText) findViewById(R.id.reg);

        marka1.requestFocus();

        odustani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent odustani = new Intent(getApplicationContext(),aktivna_uplata_glavna.class);
                startActivity(odustani);
            }
        });

        unos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provjera()) {
                    uslov = true;
                    unos();
                }
            }
        });

    }

    private void unos() {
            FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
            String ID = korisnik.getUid();
            DatabaseReference ref = databaseReference.child(ID).child("Poznata").child("Zadnji");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (uslov) {
                        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                        String ID = korisnik.getUid();
                        try {
                            broj = dataSnapshot.getValue(String.class);
                        } catch (Exception e) {
                            broj = "0";
                        }
                        if (broj == null) {
                            broj = "1";}
                        else{
                            broj = String.valueOf(Integer.parseInt(broj)+1).toString();
                        }
                        DatabaseReference ref = databaseReference.child(ID);
                        ref.child("Poznata").child("Marka").child(broj).setValue(markaK);
                        ref.child("Poznata").child("Model").child(broj).setValue(modelK);
                        ref.child("Poznata").child("Registracija").child(broj).setValue(reg);
                        ref.child("Poznata").child("Zadnji").setValue(broj);
                        Intent gotovo = new Intent(getApplicationContext(),aktivna_uplata_glavna.class);
                        startActivity(gotovo);
                        Toast.makeText(dodaj_auto.this, "Auto je dodano", Toast.LENGTH_SHORT).show();
                        uslov = false;
                    }

                }

                @Override
                public void onCancelled(DatabaseError error) {

                }
            });

    }

    @Override
    public void onBackPressed() {

    }

    public boolean provjera(){
        markaK = marka1.getText() + "";
        modelK = model1.getText() + "";
        reg = reg1.getText() + "";

            //Provjera za prazna polja marke i modela
            if ((markaK == "")) {
                marka1.setError("Ovo polje ne smije biti prazno");
                return false;
            }

            if ((modelK == "")) {
                model1.setError("Ovo polje ne smije biti prazno");
                return false;
            }

            if ((reg == "")) {
                reg1.setError("Ovo polje ne smije biti prazno");
                return false;
            }

        //Ako nema prijavljenih gresaka dozvoljava nastavak programa
        return true;
    }
}
