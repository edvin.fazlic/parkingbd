package com.jutsbrcko.edvinfazlic.parkingbd;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class aktivna_uplata_glavna extends AppCompatActivity{
    private static String[] marka1 = new String[100];
    private static String[] model1 = new String[100];
    private static String[] zona1 = new String[100];
    private static String[] uplaceno = new String[100];
    private static String[] istice = new String[100];
    private static String[] marka11 = new String[100];
    private static String[] model11 = new String[100];
    private static String[] zona11 = new String[100];
    private static String[] uplaceno1 = new String[100];
    private static String[] istice1 = new String[100];
    private Adapter adapter;
    FirebaseDatabase fBaza;
    int i = 0;
    DatabaseReference bRef;
    String broj;
    private FirebaseDatabase firebaseDatabase;
    private TextView na;
    FirebaseDatabase baza = FirebaseDatabase.getInstance();
    int duzinaNiza = 0;
    final Handler hand = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aktivna_uplata_glavna);

        fBaza = FirebaseDatabase.getInstance();
        bRef = fBaza.getInstance().getReferenceFromUrl("https://parkingbd-b176e.firebaseio.com/");

        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        DatabaseReference refBaze = baza.getReference(ID);

        Button izbornik = (Button) findViewById(R.id.uplati);
        final ListView spisak = (ListView) findViewById(R.id.spisak);

        rb();

        na = (TextView) findViewById(R.id.nemaAuta);

        izbornik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dodaj = new Intent(getApplicationContext(), izbornik.class);
                startActivity(dodaj);
            }
        });

        DatabaseReference ref = firebaseDatabase.getInstance().getReferenceFromUrl("https://parkingbd-b176e.firebaseio.com/");

        hand.postDelayed( new Runnable() {
            @Override
            public void run() {
                try {
                    if (i < 5) {
                        if (prazno()) {
                            spisak.setVisibility(View.INVISIBLE);
                            na.setVisibility(View.VISIBLE);
                        } else {
                            spisak.setVisibility(View.VISIBLE);
                            na.setVisibility(View.INVISIBLE);
                        }
                        adapter = new Adapter();
                        spisak.setAdapter(adapter);
                        povlacenje(Integer.parseInt(broj));
                        prebaci();
                        adapter.notifyDataSetChanged();
                        ukloniIsteklo();
                        hand.postDelayed(this, 500);
                        i++;
                    }
                } catch (NumberFormatException e) {
                    hand.postDelayed(this,500);
                    e.printStackTrace();
                }
            }
        }, 500 );

        for (int j = 0; j < 100; j++) {
            if (marka1[j] != null) {
                duzinaNiza++;
            }
        }
        prebaci();
        ukloniIsteklo();
    }

    public boolean prazno(){
        for (int i=0;i<100;i++) {
            if (marka1[i] != null) {
                return false;
            }
        }
        return true;
    }

    public void prebaci(){
        int j = 0;
        for (int i=0;i<100;i++) {
            if ((marka1[i] != null)) {
                marka11[j] = marka1[i];
                model11[j] = model1[i];
                zona11[j] = zona1[i];
                uplaceno1[j] = uplaceno[i];
                istice1[j] = istice[i];
                j++;
            }
        }
    }

    class Adapter extends BaseAdapter{

        @Override
        public int getCount() {
            int j = 0;
            for (int i=0;i<100;i++){
                if (marka1[i] != null){
                    j++;
                }
            }
            return j;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.activity_izgled_spiska,null);
            TextView marka = (TextView)convertView.findViewById(R.id.marka1);
            TextView model = (TextView)convertView.findViewById(R.id.model1);
            TextView zona = (TextView)convertView.findViewById(R.id.zona);
            TextView Suplaceno = (TextView)convertView.findViewById(R.id.vrijeme);
            TextView Spreostalo = (TextView)convertView.findViewById(R.id.vrijeme2);
            Button dugme = (Button) convertView.findViewById(R.id.dugme);
            dugme.setVisibility(View.INVISIBLE);
            marka.setText(marka11[i]);
            model.setText(model11[i]);
            zona.setText(zona11[i]);
            Suplaceno.setText("Uplaćeno: "+uplaceno1[i]);
            Spreostalo.setText("Ističe: "+istice1[i]);

            return convertView;
        }
    }

    public void povlacenje(int broj) {
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        for (int i = 1; i < broj; i++) {
            String rb = String.valueOf(i).toString();
            final int finalI = i;

            DatabaseReference ref = bRef.child(ID).child("Uplacen").child("Marka").child(rb);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    marka1[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //Povlacenje svih modela
            DatabaseReference ref2 = bRef.child(ID).child("Uplacen").child("Model").child(rb);
            ref2.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    model1[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            DatabaseReference ref3 = bRef.child(ID).child("Uplacen").child("Zona").child(rb);
            ref3.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    zona1[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            try {

                DatabaseReference minV = bRef.child(ID).child("Uplacen").child("Vrijeme").child(""+finalI).child("uplaceno");
                minV.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String vri = dataSnapshot.getValue(String.class);
                        uplaceno[finalI-1]=vri;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                DatabaseReference sekV = bRef.child(ID).child("Uplacen").child("Vrijeme").child(""+finalI).child("istice");
                sekV.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String vri = dataSnapshot.getValue(String.class);
                        istice[finalI-1]=vri;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void rb() {
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        DatabaseReference ref = bRef.child(ID).child("Uplacen").child("Zadnji");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                String ID = korisnik.getUid();
                try {
                    broj = dataSnapshot.getValue(String.class);
                } catch (Exception e) {
                    broj = "0";
                }
                if (broj == null) {
                    broj = "1";}
                else {
                broj = String.valueOf(Integer.parseInt(broj)+1).toString();
                }
                povlacenje(Integer.parseInt(broj));
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }

    public void ukloniIsteklo(){
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        final String ID = korisnik.getUid();
        for (int i=0;i<100;i++){
            if (istice[i] != null) {

                String istice1 = istice[i];
                String isticeSat = istice1.substring(0,5);
                String isticeDan = istice1.substring(6);

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat formatV = new SimpleDateFormat("HH:mm");
                SimpleDateFormat formatD = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat formatVD = new SimpleDateFormat("HH:mm dd/MM/yyyy");
                String vri = formatV.format(cal.getTime());
                String dat = formatD.format(cal.getTime());

                Date ocitanoDV = formatVD.parse(isticeSat+" "+isticeDan,new ParsePosition(0));
                Date trenutnoDV = formatVD.parse(vri+" "+dat,new ParsePosition(0));


                if (trenutnoDV.after(ocitanoDV)){
                    DatabaseReference ref1 = bRef.child(ID).child("Uplacen").child("Marka").child((i+1)+"");
                    DatabaseReference ref2 = bRef.child(ID).child("Uplacen").child("Model").child((i+1)+"");
                    DatabaseReference ref3 = bRef.child(ID).child("Uplacen").child("Vrijeme").child((i+1)+"");
                    DatabaseReference ref4 = bRef.child(ID).child("Uplacen").child("Zona").child((i+1)+"");
                    ref1.removeValue();
                    ref2.removeValue();
                    ref3.removeValue();
                    ref4.removeValue();
                }


                adapter.notifyDataSetChanged();

            }
        }
    }


}

