package com.jutsbrcko.edvinfazlic.parkingbd;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class prijavaRegister extends AppCompatActivity {
    private Button reg, pri;
    private EditText email, sifra;
    private TextView ispis;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference databaseReference;
    private String emailT, sifraT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prijava_register);

        FirebaseDatabase.getInstance().getReference().keepSynced(true);

        pri = (Button) findViewById(R.id.pri);
        reg = (Button) findViewById(R.id.reg);

        email = (EditText) findViewById(R.id.email);
        sifra = (EditText) findViewById(R.id.sifra);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        provjeraLogin();

        pri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provjeraPolja()) {
                    prijava();
                }
            }
        });

        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (provjeraPolja()) {
                    registracija();
                }
            }
        });

    }

    private boolean provjeraPolja() {
        emailT = email.getText().toString();
        sifraT = sifra.getText().toString();
        if (emailT.equals("")){
            email.setError("Ovo polje ne smije biti prazno");
            return false;
        }
        if (sifraT.equals("")){
            sifra.setError("Ovo polje ne smije biti prazno");
            return false;
        }
        return true;
    }

    public void registracija() {
        if (sifraT.length() < 8) {
            Toast.makeText(this, "Šifra mora biti najmanje 8 znakova", Toast.LENGTH_SHORT).show();
            sifra.setText("");
        } else {
            mAuth.createUserWithEmailAndPassword(emailT, sifraT);
            mAuth.signInWithEmailAndPassword(emailT, sifraT).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user != null) {
                        FirebaseDatabase baza = FirebaseDatabase.getInstance();
                        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                        String ID = korisnik.getUid();
                        Toast.makeText(prijavaRegister.this, "Prijavljeni ste!", Toast.LENGTH_SHORT).show();
                        Intent glavna = new Intent(getApplicationContext(), aktivna_uplata_glavna.class);
                        startActivity(glavna);
                    } else {
                        Toast.makeText(prijavaRegister.this, "Greška!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    @Override
    public void onBackPressed() {

    }

    public void prijava() {
        mAuth.signInWithEmailAndPassword(emailT, sifraT).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    Toast.makeText(prijavaRegister.this, "Prijavljeni ste!", Toast.LENGTH_SHORT).show();
                    Intent glavna = new Intent(getApplicationContext(), aktivna_uplata_glavna.class);
                    startActivity(glavna);
                } else {
                    Toast.makeText(prijavaRegister.this, "Greška!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public void provjeraLogin(){
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        if (korisnik != null){
            Toast.makeText(prijavaRegister.this, "Prijavljeni ste!", Toast.LENGTH_SHORT).show();
            Intent glavna = new Intent(getApplicationContext(),aktivna_uplata_glavna.class);
            startActivity(glavna);
        }
    }

}

