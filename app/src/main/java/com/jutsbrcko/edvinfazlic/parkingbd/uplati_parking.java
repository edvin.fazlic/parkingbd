package com.jutsbrcko.edvinfazlic.parkingbd;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class uplati_parking extends AppCompatActivity {
    private Button izbornik, dodaj;
    private TextView tekstCijena, bespParTekst;
    private Spinner sAuta, sZona, sVrijeme;
    FirebaseDatabase fBaza;
    DatabaseReference bRef;
    String broj, sat, min, dan, mje, god;
    int duzinaNiza = 0, rb;
    private static String[] marka = new String[100];
    private static String[] model = new String[100];
    private static String[] marka1 = new String[100];
    private static String[] model1 = new String[100];
    private static ArrayAdapter<String> adapter;
    private FirebaseDatabase firebaseDatabase;
    private static TextView upo;
    final Handler hand = new Handler();
    final Handler hand2 = new Handler();
    private static String brojTelefona = "", tabKonacnaTekst = "";
    private static int zona = 0, vrijeme = 0;
    private static String[] markaTab = new String[100];
    private static String[] modelTab = new String[100];
    private static String[] tabKonacna = new String[100];
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uplati_parking);

        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.SEND_SMS}, 111);

        final Intent glavna = new Intent(this, izbornik.class);   //Vracanje na glavnu

        izbornik = (Button) findViewById(R.id.odustani2);
        dodaj = (Button) findViewById(R.id.uplati);

        fBaza = FirebaseDatabase.getInstance();
        bRef = fBaza.getInstance().getReferenceFromUrl("https://parkingbd-b176e.firebaseio.com/");

        bespParTekst = (TextView) findViewById(R.id.bespParTekst);

        sAuta = (Spinner) findViewById(R.id.auta);
        sZona = (Spinner) findViewById(R.id.sZona);
        sVrijeme = (Spinner) findViewById(R.id.sVrijeme);

        upo = (TextView) findViewById(R.id.upozorenje);

        dodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //Komande za dugme dodaj
                provjeri();
            }
        });

        izbornik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {   //Komande za dugme izbornik
                startActivity(glavna);
            }
        });

        povlacenje(100);

        hand.postDelayed(new Runnable() {
            int n = 0;

            @Override
            public void run() {
                if (i < 3) {
                    try {
                        if (prazno(marka)) {
                            dodaj.setEnabled(false);
                            upo.setVisibility(View.VISIBLE);
                        } else {
                            dodaj.setEnabled(true);
                            upo.setVisibility(View.INVISIBLE);
                        }
                        povlacenje(100);
                        prebaci();
                        puni();
                        adapter.notifyDataSetChanged();
                        hand.postDelayed(this, 500);
                        i++;
                    } catch (NumberFormatException e) {
                        hand.postDelayed(this, 500);
                        e.printStackTrace();
                    }
                }
            }
        }, 500);
    }



    private void provjeri(){

        boolean uplati = true;

        Calendar kalendar = Calendar.getInstance();
        int day = kalendar.get(Calendar.DAY_OF_WEEK);

        Calendar danas = Calendar.getInstance();
        SimpleDateFormat formatV = new SimpleDateFormat("HH:mm");

        Date vrijemeSada = danas.getTime();

        int trenutno = Calendar.getInstance().get(Calendar.HOUR_OF_DAY); //Current hour

        int navecer = 21;
        int ujutru = 7;
        int subota = 14;

        switch (day) {
            case Calendar.MONDAY:
                if (trenutno >= navecer || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do utorka ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.TUESDAY:
                if (trenutno >= navecer || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do srijede ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.WEDNESDAY:
                if (trenutno >= navecer || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do cetvrtka ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.THURSDAY:
                if (trenutno >= navecer || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do petka ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.FRIDAY:
                if (trenutno >= navecer || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do subote ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.SATURDAY:
                if (trenutno >= subota || trenutno <= ujutru){
                    bespParTekst.setText("Parking je besplatan do ponedeljka ujutro u 7 sati.");
                    uplati = false;
                }
                break;
            case Calendar.SUNDAY:
                bespParTekst.setText("Parking je besplatan do ponedeljka ujutro u 7 sati.");
                uplati = false;
                break;
        }
        if (uplati) {
            bespParTekst.setVisibility(View.INVISIBLE);
            rb();
            stavi();

        }else{
            bespParTekst.setVisibility(View.VISIBLE);
        }
    }

    private void rb() {
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        DatabaseReference ref = bRef.child(ID).child("Uplacen").child("Zadnji");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                String ID = korisnik.getUid();
                try {
                    broj = dataSnapshot.getValue(String.class);
                } catch (Exception e) {
                    broj = "0";
                }
                if (broj == null) {
                    broj = "1";
                } else {
                    broj = String.valueOf(Integer.parseInt(broj) + 1).toString();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        for (int i = 1; i < 100; i++) {
            final int finalI = i;
            try {
                DatabaseReference refTabliceMa = bRef.child(ID).child("Poznata").child("Marka").child(finalI + "");
                DatabaseReference refTabliceMo = bRef.child(ID).child("Poznata").child("Model").child(finalI + "");
                DatabaseReference refTabliceTa = bRef.child(ID).child("Poznata").child("Registracija").child(finalI + "");
                refTabliceMa.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String vri = dataSnapshot.getValue(String.class);
                        markaTab[finalI - 1] = vri;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                refTabliceMo.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String vri = dataSnapshot.getValue(String.class);
                        modelTab[finalI - 1] = vri;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                refTabliceTa.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String vri = dataSnapshot.getValue(String.class);
                        tabKonacna[finalI - 1] = vri;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void stavi() {    //Unos podataka u bazu
        final Intent nazad = new Intent(this, aktivna_uplata_glavna.class);

        String ID = null;

        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        ID = korisnik.getUid();
        DatabaseReference refBaze = bRef.child(ID).child("Uplacen");

        rb = sAuta.getSelectedItemPosition();

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatV = new SimpleDateFormat("HH:mm");
        SimpleDateFormat formatD = new SimpleDateFormat("dd/MM/YYYY");
        String vri = formatV.format(cal.getTime());
        String dat = formatD.format(cal.getTime());
        String[] vrijeme = vri.split(":");
        String[] datum = dat.split("/");
        dan = datum[0];
        mje = datum[1];
        god = datum[2];
        sat = vrijeme[0];
        min = vrijeme[1];


        try {
            if ((sZona.getSelectedItemId() == 0) && (sVrijeme.getSelectedItemId() == 0)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Crvena1);
            }
            if ((sZona.getSelectedItemId() == 0) && (sVrijeme.getSelectedItemId() == 1)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Crvena24);
            }
            if ((sZona.getSelectedItemId() == 1) && (sVrijeme.getSelectedItemId() == 0)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Plava1);
            }
            if ((sZona.getSelectedItemId() == 1) && (sVrijeme.getSelectedItemId() == 1)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Plava24);
            }
            if ((sZona.getSelectedItemId() == 2) && (sVrijeme.getSelectedItemId() == 0)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Zelena1);
            }
            if ((sZona.getSelectedItemId() == 2) && (sVrijeme.getSelectedItemId() == 1)) {
                brojTelefona = "0" + getResources().getInteger(R.integer.Zelena24);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 100; i++) {
            try {

                if ((markaTab[i] == null) || (modelTab[i] == null)) {
                    continue;
                }

                if ((markaTab[i] + " " + modelTab[i]).equalsIgnoreCase(sAuta.getSelectedItem().toString())) {
                    String priv = tabKonacna[i];
                    for (int a=0;a<priv.length();a++){
                        if (((priv.charAt(a)>=65) && (priv.charAt(a)<=90))||((priv.charAt(a)>=48) && (priv.charAt(a)<=57))){
                            tabKonacnaTekst += priv.charAt(a);
                        }
                    }
                }

                if (provjeriDozvolu(android.Manifest.permission.SEND_SMS)) {
                    try {
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(brojTelefona, null, tabKonacnaTekst, null, null);

                        refBaze.child("Vrijeme").child(broj).child("uplaceno").setValue(sat + ":" + min + " " + dan + "/" + mje + "/" + god);

                        if (sVrijeme.getSelectedItemId() == 0) {
                            if (Integer.parseInt(sat) + 1 < 21) {
                                sat = Integer.parseInt(sat) + 1 + "";
                            } else {
                                sat = 21 + "";
                                min = "00";
                            }
                        } else {
                            sat = 21 + "";
                            min = "00";
                        }

                        if (Integer.parseInt(sat) < 10) {
                            sat = "0" + sat;
                        }

                        refBaze.child("Vrijeme").child(broj).child("istice").setValue(sat + ":" + min + "  " + dan + "/" + mje + "/" + god);

                        if (sZona.getSelectedItemId() == 0) {
                            refBaze.child("Zona").child(broj).setValue("Crvena Zona");
                        }
                        if (sZona.getSelectedItemId() == 1) {
                            refBaze.child("Zona").child(broj).setValue("Plava Zona");
                        }
                        if (sZona.getSelectedItemId() == 2) {
                            refBaze.child("Zona").child(broj).setValue("Zelena Zona");
                        }

                        refBaze.child("Marka").child(broj).setValue(marka1[rb]);
                        refBaze.child("Model").child(broj).setValue(model1[rb]);
                        refBaze.child("Zadnji").setValue(broj);

                        Toast.makeText(this, "Uplata je poslana,ocekujte potvrdu", Toast.LENGTH_SHORT).show();

                        break;
                    } catch (Exception e) {
                        Toast.makeText(this, "Uplata nije uspjela", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(this, "Nece", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        Handler hand = new Handler();
        hand.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(nazad);

            }
        }, 1500);
    }

    public boolean provjeriDozvolu(String dozvola){
        int check = ContextCompat.checkSelfPermission(this,dozvola);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    public void prebaci(){
        int j = 0;
        for(int i = 0;i<100;i++)
        {
            if ((marka[i] != null)) {
                marka1[j] = marka[i];
                model1[j] = model[i];
                j++;
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void povlacenje(int broj) {
        rb();
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        for (int i = 0; i < broj; i++) {
            String rb = String.valueOf(i+1).toString();
            final int finalI = i;
            DatabaseReference ref = bRef.child(ID).child("Poznata").child("Marka").child(rb);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    marka[finalI] = vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //Povlacenje svih modela
            DatabaseReference ref2 = bRef.child(ID).child("Poznata").child("Model").child(rb);
            ref2.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    model[finalI] = vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void puni() {
        duzinaNiza = 0;
        for (int j = 0; j < 100; j++) {
            if (marka[j] != null) {
                duzinaNiza++;
            }
        }

        String[] markaModel = new String[duzinaNiza];

        int j = 0;
        for (int i = 0; i < 100; i++) {
            if ((marka[i] != null) && (model[i] != null)) {
                markaModel[j] = marka[i] + " " + model[i];
                j++;
            }
        }

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, markaModel);
        sAuta.setAdapter(adapter);

    }


    public boolean prazno(String[] marka){
        for (int i=0;i<100;i++){
            if (marka[i] != null){
                return false;
            }
        }
        return true;
    }
}
