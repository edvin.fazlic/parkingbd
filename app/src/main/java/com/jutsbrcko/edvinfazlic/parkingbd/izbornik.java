package com.jutsbrcko.edvinfazlic.parkingbd;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class izbornik extends AppCompatActivity {
    public Button nula,uplati,dodajauto,glavna,spisak;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izbornik);

        dodajauto = (Button) findViewById(R.id.dodajaA);
        uplati = (Button) findViewById(R.id.uplatiA);
        nula = (Button) findViewById(R.id.odjavaA);
        glavna = (Button) findViewById(R.id.glavnaA);
        spisak = (Button) findViewById(R.id.spisakA);

        final Intent uplatiForma = new Intent(this,uplati_parking.class);
        final Intent spisakForma = new Intent(this,spisak_auta.class);
        final Intent dodajForma = new Intent(this,dodaj_auto.class);
        final Intent novi = new Intent(this, prijavaRegister.class);

        dodajauto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(dodajForma);
            }
        });

        spisak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(spisakForma);
            }
        });

        uplati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(uplatiForma);
            }
        });

        nula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Handler hand = new Handler();
                hand.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                        if (korisnik == null) {
                            startActivity(novi);
                        }
                    }
                }, 1500);
            }
        });

        glavna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent novi = new Intent(getApplicationContext(),aktivna_uplata_glavna.class);
                startActivity(novi);
            }
        });

    }

    @Override
    public void onBackPressed() {
    }
}
