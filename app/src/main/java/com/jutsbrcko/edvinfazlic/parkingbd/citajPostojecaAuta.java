package com.jutsbrcko.edvinfazlic.parkingbd;

public class citajPostojecaAuta {
    private String Marka;
    private String Model;
    private String Registracija;

    citajPostojecaAuta(){

    }

    public String getMarka() {
        return Marka;
    }

    public void setMarka(String marka) {
        Marka = marka;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getRegistracija() {
        return Registracija;
    }

    public void setRegistracija(String registracija) {
        Registracija = registracija;
    }
}
