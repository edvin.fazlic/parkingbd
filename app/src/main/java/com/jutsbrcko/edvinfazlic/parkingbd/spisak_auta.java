package com.jutsbrcko.edvinfazlic.parkingbd;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Property;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class spisak_auta extends AppCompatActivity {
    FirebaseDatabase fBaza;
    DatabaseReference bRef;
    private String broj;
    private ListView spisak;
    private Button izbornik;
    private TextView na;
    private static String[] marka = new String[100];
    private static String[] model = new String[100];
    private static String[] registracija = new String[100];
    private static String[] marka1 = new String[100];
    private static String[] model1 = new String[100];
    private static String[] registracija1 = new String[100];
    private CustomAdapter customAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spisak_auta);

        fBaza = FirebaseDatabase.getInstance();
        bRef = fBaza.getInstance().getReferenceFromUrl("https://parkingbd-b176e.firebaseio.com/");

        izbornik = (Button) findViewById(R.id.izbornik);

        final Intent izb = new Intent(getApplicationContext(), izbornik.class);

        izbornik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(izb);
            }
        });

        spisak = (ListView) findViewById(R.id.spisakAuta);
        na = (TextView) findViewById(R.id.nemaAuta);

        rb();

        final Handler hand = new Handler();
        hand.postDelayed( new Runnable() {
            @Override
            public void run() {
                try {
                    if (prazno()){
                        spisak.setVisibility(View.INVISIBLE);
                        na.setVisibility(View.VISIBLE);
                    }else{
                        spisak.setVisibility(View.VISIBLE);
                        na.setVisibility(View.INVISIBLE);
                    }
                    customAdapter = new CustomAdapter();
                    spisak.setAdapter(customAdapter);
                    povlacenje(Integer.parseInt(broj));
                    prebaci();
                    customAdapter.notifyDataSetChanged();
                    hand.postDelayed( this,  500 );
                } catch (Exception e) {
                    hand.postDelayed(this,500);
                    e.printStackTrace();
                }
            }
        },  500 );
    }

    public boolean prazno(){
        for (int i=0;i<100;i++) {
            if (marka[i] != null) {
                return false;
            }
        }
        return true;
    }

    public void prebaci(){
        int j = 0;
        for (int i=0;i<100;i++) {
            if ((marka[i] != null)) {
                marka1[j] = marka[i];
                model1[j] = model[i];
                registracija1[j] = registracija[i];
                j++;
            }
        }
    }

    private void rb() {
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        DatabaseReference ref = bRef.child(ID).child("Poznata").child("Zadnji");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
                    String ID = korisnik.getUid();
                    try {
                        broj = dataSnapshot.getValue(String.class);
                    } catch (Exception e) {
                        broj = "0";
                    }
                    if (broj == null) {
                        broj = "1";}
                    else{
                        broj = String.valueOf(Integer.parseInt(broj)+1).toString();

                        //Toast.makeText(spisak_auta.this, broj, Toast.LENGTH_SHORT).show();
                    }
                    povlacenje(Integer.parseInt(broj));
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

    }



    public void povlacenje(int broj) {
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        String ID = korisnik.getUid();
        for (int i = 1; i < broj; i++) {
            String rb = String.valueOf(i).toString();
            final int finalI = i;
            DatabaseReference ref = bRef.child(ID).child("Poznata").child("Marka").child(rb);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    marka[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            //Povlacenje svih modela
            DatabaseReference ref2 = bRef.child(ID).child("Poznata").child("Model").child(rb);
            ref2.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    model[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            //Povlacenje svih tablica
            DatabaseReference ref3 = bRef.child(ID).child("Poznata").child("Registracija").child(rb);
            ref3.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String vri = dataSnapshot.getValue(String.class);
                    registracija[finalI-1]=vri;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            int j = 0;
            for (int i=0;i<100;i++){
                if (marka[i] != null){
                    j++;
                }
            }
            return j;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.activity_izgled_spiska,null);
            final TextView Smarka = (TextView)convertView.findViewById(R.id.marka1);
            TextView Smodel = (TextView)convertView.findViewById(R.id.model1);
            TextView Szona = (TextView)convertView.findViewById(R.id.zona);
            TextView Svrijeme = (TextView)convertView.findViewById(R.id.vrijeme);
            TextView Spreostalo = (TextView)convertView.findViewById(R.id.vrijeme2);
            Button dugme = (Button) convertView.findViewById(R.id.dugme);
            Smarka.setText(marka1[i]+"");
            Smodel.setText(model1[i]+"");
            Szona.setText(registracija1[i]+"");
            Svrijeme.setText("");
            Spreostalo.setText("");
            dugme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    brisanje(i);
                }
            });
            return convertView;
        }
    }

    public void brisanje(int i){
        int mjesto = 0;
        FirebaseUser korisnik = FirebaseAuth.getInstance().getCurrentUser();
        final String ID = korisnik.getUid();
        for (int brojac=0;brojac<100;brojac++){
            if (marka[brojac] == marka1[i]){
                mjesto = brojac+1;
            }
        }
        DatabaseReference ref1 = bRef.child(ID).child("Poznata").child("Marka").child(mjesto+"");
        DatabaseReference ref2 = bRef.child(ID).child("Poznata").child("Model").child(mjesto+"");
        DatabaseReference ref3 = bRef.child(ID).child("Poznata").child("Registracija").child(mjesto+"");
        ref1.removeValue();
        ref2.removeValue();
        ref3.removeValue();
    }


    @Override
    public void onBackPressed() {

    }

}
